import 'isomorphic-fetch';
import { Events } from './../enums/events';
import { EventAggregator } from 'aurelia-event-aggregator';
import {Configuration} from '../configuration';
import {autoinject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';

@autoinject
export class WebApi {
    constructor(
        private httpClient: HttpClient,
        private configuration: Configuration,
        private eventAggregator: EventAggregator) {
        httpClient.configure(config => {
            config
                .withBaseUrl(this.configuration.baseUri + '/api/')
                .withDefaults({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .rejectErrorResponses()
                .withInterceptor({
                    request: (request) => {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request; // you can return a modified Request, or you can short-circuit the request by returning a Response
                    },
                    response: (response) => {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response; // you can return a modified Response
                    },
                    responseError: (error: any, request?: Request) => {
                        console.log('error status: ', error);
                        if (error.status === 401) {
                            console.log('----------- 401 -------------', Events.Logout);
                            this.eventAggregator.publish(Events.Logout);                            
                        }
                        return error;
                    }                    
                });
        });
    }

    async getAll(path: string) {
        let token = localStorage.getItem('chnt');
        const result = await this.httpClient.fetch(path, {
            method: 'get',
            headers: {
                'Authorization' : token
            },
            mode: 'cors'
        }).then(response => response.json());
        return result;
    }

    async get(path: string) {
        let token = localStorage.getItem('chnt');
        const result = await this.httpClient.fetch(path, {
            method: 'get',
            headers: {
                'Authorization' : token
            },
            mode: 'cors'
        }).then(response => response.json());
        return result;
    }

    async post(path: string, payload: any) {
      let token = localStorage.getItem('chnt');
      const result = await this.httpClient.fetch(path, {
        method: 'post',
        body: JSON.stringify(payload),
        headers: {
            'Authorization' : token
        },
        mode: 'cors'
      }).then(response => response.json());
      return result;
    }

    async put(path: string, payload: any) {
      let token = localStorage.getItem('chnt');
      const result = await this.httpClient.fetch(path, {
        method: 'put',
        body: JSON.stringify(payload),
        headers: {
            'Authorization' : token
        },
        mode: 'cors'
      }).then(response => response.json());
      return result;
    }    
}
