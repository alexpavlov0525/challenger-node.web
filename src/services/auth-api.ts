import 'isomorphic-fetch';
import { Events } from './../enums/events';
import {Configuration} from '../configuration';
import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject('AuthHttpClient', Configuration, EventAggregator)
export class AuthApi {
    constructor(
        private httpClient: HttpClient,
        private configuration: Configuration,
        private eventAggregator: EventAggregator) {
        httpClient.configure(config => {
            config
                .withBaseUrl(this.configuration.baseUri + '/authentication/')
                .withDefaults({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .withInterceptor({
                    request: (request) => {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request; // you can return a modified Request, or you can short-circuit the request by returning a Response
                    },
                    response: (response) => {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response; // you can return a modified Response
                    },
                    responseError: (error: any, request?: Request) => {
                        if (error.status === 401) {
                            this.eventAggregator.publish(Events.Logout);                            
                        }
                        return error;
                    }
                });
        });
    }

    async getAll(path: string) {
        const result = await this.httpClient.fetch(path, {
            method: 'get',
        }).then(response => response.json());
        return result;
    }

    async post(path: string, payload: any) {
      const result = await this.httpClient.fetch(path, {
        method: 'post',
        body: JSON.stringify(payload)
      }).then(response => response.json());
      return result;
    }
}
