import { bindable } from 'aurelia-framework';
import { customElement } from "aurelia-templating";
import * as mousetrap from 'mousetrap';

@customElement('number-control')
export class NumberControl{
    @bindable required;
    @bindable label;
    @bindable value;    
    @bindable max;
    @bindable min;
    @bindable disabled;
    @bindable property;

    formGroupControl: HTMLElement | null;
    messageControl: HTMLElement | null;
    errorMessages: string [] = [];
    parentVm: any;

    async bindKeys() {
        let blockChars = 'qwertyuiop[]asdfghjkl;zxcvbnm,./\'"`~!@#$%^&*()_+-={}|\\:<>?';
        let inputControl = document.getElementById(`${this.property}_input`) as Element;
        blockChars.split('').forEach((key) => {
            mousetrap(inputControl).bind(key, (e) => {
                e.preventDefault();
            })
        });
    }

    async attached() {
        await this.bindKeys();
        this.formGroupControl = document.getElementById(`${this.property}_form_group`);
        this.messageControl = document.getElementById(`${this.property}_messages`);
        
        if (this.parentVm) {
            this.parentVm.registerChild(this);
        }
    }

    bind(bindingContext: any) {
        this.parentVm = bindingContext;
    }

    async validate(): Promise<boolean> {
        let isValid = true;
        this.errorMessages = [];
        this.removeErrors();

        if (this.required) {
            if (!this.value) {
                isValid = false;
                this.errorMessages.push('required');
            }
        }

        if (this.max) {
            if (this.value && Number(this.value) > this.max) {
                isValid = false;
                this.errorMessages.push(`has to be less than ${this.max}`);
            }
        }

        if (this.min) {
            if (this.value && Number(this.value) < this.min) {
                isValid = false;
                this.errorMessages.push(`has to be more than ${this.min}`);
            }
        }

        if (!isValid) {
            this.showErrors();
        }

        return isValid;
    }

    protected removeErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.remove('has-error'); 

        if (this.messageControl) {
            while (this.messageControl.hasChildNodes()) {
                let lastChild = this.messageControl.lastChild;
                if (lastChild) {
                    this.messageControl.removeChild(lastChild);
                }
            }
        }
    }

    protected showErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.add('has-error');

        if (this.errorMessages && this.messageControl) {
            let self = this;
            this.errorMessages.forEach((message) => {
                let error = document.createElement('p');
                error.innerText = message;
                error.classList.add('red-font');
                if (self.messageControl) {
                    self.messageControl.appendChild(error);           
                }
            });
        }
    }

    async valueChanged(newValue: string) {         
        await this.validate();
    }

    async minChanged() {
        await this.validate();
    }

    async maxChanged() {
        await this.validate();
    }
}