import { bindable } from 'aurelia-framework';
import { customElement } from "aurelia-templating";

@customElement('text-control')
export class TextControl{
    @bindable required;
    @bindable label;
    @bindable value;    
    @bindable maxLength;
    @bindable disabled;
    @bindable property;

    formGroupControl: HTMLElement | null;
    messageControl: HTMLElement | null;
    errorMessages: string [] = [];
    parentVm: any;

    async attached() {
        this.formGroupControl = document.getElementById(`${this.property}_form_group`);
        this.messageControl = document.getElementById(`${this.property}_messages`);
        
        if (this.parentVm) {
            this.parentVm.registerChild(this);
        }
    }

    bind(bindingContext: any) {
        this.parentVm = bindingContext;
    }

    async validate(): Promise<boolean> {
        let isValid = true;
        this.errorMessages = [];
        this.removeErrors();

        if (this.required) {
            if (!this.value) {
                isValid = false;
                this.errorMessages.push('required');
            }
        }

        if (this.maxLength) {
            if (this.value && this.value.length > this.maxLength) {
                isValid = false;
                this.errorMessages.push(`can not be longer than ${this.maxLength} charachters`);
            }
        }

        if (!isValid) {
            this.showErrors();
        }

        return isValid;
    }

    protected removeErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.remove('has-error'); 

        if (this.messageControl) {
            while (this.messageControl.hasChildNodes()) {
                let lastChild = this.messageControl.lastChild;
                if (lastChild) {
                    this.messageControl.removeChild(lastChild);
                }
            }
        }
    }

    protected showErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.add('has-error');

        if (this.errorMessages && this.messageControl) {
            let self = this;
            this.errorMessages.forEach((message) => {
                let error = document.createElement('p');
                error.innerText = message;
                error.classList.add('red-font');
                if (self.messageControl) {
                    self.messageControl.appendChild(error);           
                }
            });
        }
    }

    async valueChanged(newValue: string) {
        await this.validate();
    }
}