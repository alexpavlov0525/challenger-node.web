import { bindable } from 'aurelia-framework';
import { customElement } from "aurelia-templating";

@customElement('select-control')
export class SelectControl{
    @bindable required;
    @bindable label;
    @bindable value;   
    @bindable entityId; 
    @bindable disabled;
    @bindable property;
    @bindable items;

    formGroupControl: HTMLElement | null;
    messageControl: HTMLElement | null;
    errorMessages: string [] = [];
    parentVm: any;

    async attached() {
        this.formGroupControl = document.getElementById(`${this.property}_form_group`);
        this.messageControl = document.getElementById(`${this.property}_messages`);
        
        if (this.parentVm) {
            this.parentVm.registerChild(this);
        }
    }

    bind(bindingContext: any) {
        this.parentVm = bindingContext;
        if (this.items) {
            this.items.unshift({key: null, value: 'Select'});
            this.value =  this.items[0].value;
            this.entityId = this.items[0].key;
        }
    }

    select(item: any) {
        if (item) {
            this.value = item.value;
            this.entityId = item.key;
        }
    }

    async validate(): Promise<boolean> {
        let isValid = true;
        this.errorMessages = [];
        this.removeErrors();

        if (this.required) {
            if (!this.entityId) {
                isValid = false;
                this.errorMessages.push('required');
            }
        }

        if (!isValid) {
            this.showErrors();
        }

        return isValid;
    }

    protected removeErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.remove('has-error'); 

        if (this.messageControl) {
            while (this.messageControl.hasChildNodes()) {
                let lastChild = this.messageControl.lastChild;
                if (lastChild) {
                    this.messageControl.removeChild(lastChild);
                }
            }
        }
    }

    protected showErrors() {
        if (!this.formGroupControl) {
            return;
        }
        this.formGroupControl.classList.add('has-error');

        if (this.errorMessages && this.messageControl) {
            let self = this;
            this.errorMessages.forEach((message) => {
                let error = document.createElement('p');
                error.innerText = message;
                error.classList.add('red-font');
                if (self.messageControl) {
                    self.messageControl.appendChild(error);           
                }
            });
        }
    }

    async valueChanged(newValue: string) {
        await this.validate();
    }
}