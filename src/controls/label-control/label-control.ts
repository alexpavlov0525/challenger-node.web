import { bindable } from 'aurelia-framework';
import { customElement } from "aurelia-templating";

@customElement('label-control')
export class LabelControl{
    @bindable label;
    @bindable value;    
    // @bindable property;

    // formGroupControl: HTMLElement | null;
    // messageControl: HTMLElement | null;
    // errorMessages: string [] = [];
    // parentVm: any;

    async attached() {
        // this.formGroupControl = document.getElementById(`${this.property}_form_group`);
        // this.messageControl = document.getElementById(`${this.property}_messages`);
        
        // if (this.parentVm) {
        //     this.parentVm.registerChild(this);
        // }
    }

    bind(bindingContext: any) {
        // this.parentVm = bindingContext;
    }
}