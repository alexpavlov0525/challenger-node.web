export class RoundFormatValueConverter {
  toView(value) {
      if (value) {
        return Math.round(value);
      } else {
          return 0;
      }
  }
}