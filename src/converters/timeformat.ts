import * as moment from 'moment';

export class TimeFormatValueConverter {
  toView(value) {
    if (value) {
      return moment(value).format('h:mm:ss a');
    } else {
      return null;
    }
  }
}