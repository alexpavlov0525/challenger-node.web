import * as moment from 'moment';

export class DateFormatValueConverter {
  toView(value) {
      if (value) {
        return moment(value).format('M/D/YYYY');
      } else {
          return null;
      }
  }
}