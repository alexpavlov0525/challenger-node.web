export class Configuration {
    baseUri: string;
    notificationBaseUri: string;
    constructor() {
      this.baseUri = process.env.NODE_ENV === 'development' ? 'http://localhost:5001' : 'https://rest.challengernode.com';
      this.notificationBaseUri = process.env.NODE_ENV === 'development' ? 'http://localhost:6001' : 'https://notification.challengernode.com';
    }
}
