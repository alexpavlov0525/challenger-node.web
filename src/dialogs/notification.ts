import { NotificationDialog } from './notification-dialog/notification-dialog';
import { DialogService } from 'aurelia-dialog';
import { autoinject } from 'aurelia-framework';

@autoinject
export class Notification {
    constructor(private dialogService: DialogService) {
    }

    info(payload: any) {
        this.dialogService
            .open({ viewModel: NotificationDialog, model: payload })
    }
}