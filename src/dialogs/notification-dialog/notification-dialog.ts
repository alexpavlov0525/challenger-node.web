import { NotificationRepository } from './../../repositories/notificationRepository';
import { DialogController } from 'aurelia-dialog';
import { autoinject } from 'aurelia-framework';
import { MessageType } from "../../enums/message-type-enum";

@autoinject
export class NotificationDialog {
    notification: any;

    constructor(
        private controller: DialogController,
        private notificationRepository: NotificationRepository) {

    }

    activate(payload: any) {
        this.notification = payload;
        setTimeout(() => {
            this.controller.cancel();
        }, 10000);
    }

    async accept() {
        if (this.notification.messageType === MessageType.JoinGame) {
            let response = await this.notificationRepository.join(this.notification.gameId, { notificationId: this.notification.notificationId });
        }

        if (this.notification.messageType === MessageType.StartRound) {
            let response = await this.notificationRepository.startRound(this.notification.gameId, this.notification.round, { notificationId: this.notification.notificationId });
        }

        if (this.notification.messageType === MessageType.FinishRound) {
            let response = await this.notificationRepository.finishRound(this.notification.gameId, this.notification.round, { notificationId: this.notification.notificationId });
        }

        this.controller.ok();
    }

    async decline() {

    }
}