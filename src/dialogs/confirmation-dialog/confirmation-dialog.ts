import { DialogController } from 'aurelia-dialog';
import { autoinject } from 'aurelia-framework';

@autoinject
export class ConfirmationDialog {
    message: string;

    constructor(private controller: DialogController) {
        
    }

    activate(message: string) {
        this.message = message;
    }
}