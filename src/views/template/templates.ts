import { Router } from 'aurelia-router';
import { bindable, autoinject } from 'aurelia-framework';
import { TemplateRepository } from './../../repositories/templateRepository';
import { Template } from './../../models/template';

@autoinject
export class Templates {
    filteredTemplates: Template [] = [];
    templates: Template[];
    @bindable keyword: string;

    constructor(
        private templateRepository: TemplateRepository,
        private router: Router) {

    }

    async activate(params, routeConfig, navigationInstruction) {
        this.templates = await this.templateRepository.getAll();
        this.filteredTemplates = this.templates;
    }

    keywordChanged(newValue: string) {
        if (newValue) {
            this.filteredTemplates = 
                this.templates.filter(
                    x => {
                            if (x && x.name) {
                                return x.name.toLocaleLowerCase()
                                    .indexOf(newValue.toLocaleLowerCase()) > -1
                                }
                            });
                            
        } else {
            this.filteredTemplates = this.templates;
        }
    }

    edit(id: string) {
        this.router.navigateToRoute('template', {id: id});
    }

    add() {
        this.router.navigateToRoute('template-add');
    }

}