import { User } from './../../models/user';
import { Router } from 'aurelia-router';
import { autoinject } from 'aurelia-framework';
import { Template } from './../../models/template';
import { TemplateRepository } from './../../repositories/templateRepository';
import { EntityViewModel } from '../../abstract/entityViewModel';

@autoinject
export class TemplateEdit extends EntityViewModel {
    template: Template | undefined;
    canEdit: boolean = true;

    constructor(private templateRepository: TemplateRepository,
                private router: Router,
                private user: User) {
        super();
    }

    async activate(params: any) {

        if (!params.id) {
            this.isAddingNew = true;
        } else {
            this.entityId = params.id;
            this.template = await this.templateRepository.get(params.id);
            if (this.template) {
                this.canEdit = this.user.username === this.template.createdBy;
            }
        }
    }

    cancel() {
        this.router.navigateToRoute('templates');
        this.forceDeactivate = true;
    }

    async save() {
        this.forceDeactivate = false;
        this.canBeDeactivated = true;
        let isValid = await this.validateChildren();

        let response;

        if (isValid) {
            if (this.template) {
                this.template.createdBy = this.user.username;
                if (!this.template.roundsToWin) {
                    this.template.roundsToWin = 1;
                }
            }
            if (this.isAddingNew) {
                response = await this.templateRepository.insert(this.template);
            } else {
                response =  await this.templateRepository.update(this.entityId, this.template);
            }

            if (response.success) {
                this.router.navigateToRoute('templates');
            } else {
                this.canBeDeactivated = false;
                this.errors = response.errors;
            }
        }
    }

}