import { Router } from 'aurelia-router';
import { autoinject } from 'aurelia-framework';

@autoinject
export class Welcome {
    constructor(private router: Router) {
        // 
    }
    toLogin() {
        this.router.navigateToRoute('dashboard');
    }
}