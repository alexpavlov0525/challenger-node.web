import { System } from './../../models/system';
import { UserRepository } from './../../repositories/user/userRepository';
import { SystemRepository } from './../../repositories/systemRepository';
import { User } from './../../models/user';
import {autoinject, bindable} from 'aurelia-framework';
import {AuthRepository} from '../../repositories/authentication/auth-repository';
import {Router} from 'aurelia-router';
import * as mousetrap from 'mousetrap';

@autoinject
export class Login {
    bindCreateNew: any;
    canCreate: boolean = false;
    confirmPassword: string;
    errorMessage: string | null;
    @bindable user: any;

    constructor(
        private authRepository: AuthRepository,
        private userRepository: UserRepository,
        private systemRepository: SystemRepository,
        private userProfile: User,
        private system: System,
        private router: Router) {
            this.user = new User();
    }

    async activate() {
    }

    userChanged() {
        this.errorMessage = null;
    }

    createNew() {
        this.canCreate = true;
        this.resetUser();
    }

    resetUser() {
        this.user.password = '';
        this.user.username = '';
        this.confirmPassword = '';
        this.errorMessage = null;
    }

    back() {
        this.canCreate = false;
        this.resetUser();
    }

    async save() {
        this.errorMessage = null;

        if (this.confirmPassword !== this.user.password) {
            this.errorMessage = 'Passwords do not match';
            return;
        }

        let result = await this.authRepository.add(this.user);

        if (!result.success) {
            this.errorMessage = result.message;
        } else {
            let token = result['token'];
            if (token) {
                localStorage.setItem('chnt', token);
                this.router.navigateToRoute('dashboard');            
                this.errorMessage = null;
            }
        }
    }

    async attached() {
        mousetrap.bind('enter', async () => {
            await this.submitLogin();
        });
        this.bindCreateNew = await this.systemRepository.getCanCreate();
        this.system.canCreate = this.bindCreateNew;
    }

    async submitLogin() {
       let result = await this.authRepository.authenticate(this.user);
       if (!result.success) {
           this.errorMessage = result.message;
       } else {
        let token = result['token'];
        if (token) {
            localStorage.setItem('chnt', token);
            this.router.navigateToRoute('dashboard');
        } 
       }
    }
}


