import { Template } from './../../models/template';
import { UserRepository } from './../../repositories/user/userRepository';
import { TemplateRepository } from './../../repositories/templateRepository';
import { Game } from './../../models/game';
import { Router } from 'aurelia-router';
import { User } from './../../models/user';
import { GameRepository } from './../../repositories/gameRepository';
import { autoinject, bindable } from 'aurelia-framework';
import { EntityViewModel } from "../../abstract/entityViewModel";

@autoinject
export class GameEdit extends EntityViewModel{
    @bindable game: any = {};
    @bindable templateId: string;
    selectedTemplate: Template;
    isTemplateSelected: boolean = false;
    playersCountDesc: string = 'n/a';
    roundsDesc: string = 'n/a';
    gameDesc: string | null = 'n/a';
    templates: any[];
    actualTemplates: any[];
    users: any[];
    actualUsers: any[];

    constructor(
        private gameRepository: GameRepository,
        private templateRepository: TemplateRepository,
        private userRepository: UserRepository,
        private router: Router,
        private user: User) {
        super();
    }

    async activate(params: any) {
        this.actualTemplates = await this.templateRepository.getAll();
        this.templates = 
            this.actualTemplates.map(template => {return {key: template._id, value: template.name}});
        this.actualUsers = await this.userRepository.getAllUsernames();
        this.users = this.actualUsers.map((user) => {return {key: user, value: user, selected: false}});

        if (!params.id) {
            this.isAddingNew = true;
        } else {
            this.entityId = params.id;
            this.game = await this.gameRepository.get(params.id);
        }
    }

    templateIdChanged(newValue: string) {
        if (newValue) {
            this.selectedTemplate = this.actualTemplates.find(x => x._id === newValue);
            console.log('selected template', this.selectedTemplate);
            this.playersCountDesc = `from ${this.selectedTemplate.minPlayers} to ${this.selectedTemplate.maxPlayers} can play this game`;
            this.roundsDesc = `need to win ${this.selectedTemplate.roundsToWin} round(s) out of ${this.selectedTemplate.rounds} round(s)`;
            this.gameDesc = this.selectedTemplate.description;
            this.isTemplateSelected = true;
        } else {
            this.isTemplateSelected = false;
            this.playersCountDesc = 'n/a';
            this.roundsDesc = 'n/a';
            this.gameDesc = 'n/a';
        }

        this.game.templateId = newValue;
    }

    cancel() {
        this.router.navigateToRoute('games');
        this.forceDeactivate = true;
    }

    async save() {
        this.forceDeactivate = false;
        this.canBeDeactivated = true;
        let isValid = await this.validateChildren();

        if (isValid) {
            this.users = this.users.filter(x => x.selected);
            this.game.players = this.users.map(x => {return x.value});
            this.game.players.unshift(this.user.username);

            let response = await this.gameRepository.insert(this.game);
            
            if (response.success) {
                this.router.navigateToRoute('games');
            } else {
                this.canBeDeactivated = false;
                this.errors = [];
                this.errors.push(response.message);
            }
        }
    }
}