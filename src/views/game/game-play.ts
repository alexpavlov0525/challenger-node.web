import { Events } from './../../enums/events';
import { EventAggregator } from 'aurelia-event-aggregator';
import { GameStatus } from './../../enums/game-status-enum';
import { ConfirmationDialog } from './../../dialogs/confirmation-dialog/confirmation-dialog';
import { Round } from './../../models/round';
import { Game } from './../../models/game';
import { DialogService } from 'aurelia-dialog';
import { User } from './../../models/user';
import { Router } from 'aurelia-router';
import { UserRepository } from './../../repositories/user/userRepository';
import { TemplateRepository } from './../../repositories/templateRepository';
import { GameRepository } from './../../repositories/gameRepository';
import { autoinject, bindable } from 'aurelia-framework';
import { EntityViewModel } from "../../abstract/entityViewModel";
import * as moment from 'moment';
import { RoundStatus } from "../../enums/round-status-enum";

@autoinject
export class GamePlay extends EntityViewModel {
    game: Game;
    @bindable status: string;
    gameId: string;
    constructor(private gameRepository: GameRepository,
                private templateRepository: TemplateRepository,
                private userRepository: UserRepository,
                private router: Router,
                private dialogService: DialogService,
                private user: User,
                private eventAggregator: EventAggregator) {
        super();
        this.setupSubscriptions();
    }

    async activate(params, routeConfig, navigationInstruction) {
        this.gameId = params.gameId;

        await this.getGame();
    }

    setupSubscriptions() {
        this.eventAggregator.subscribe(Events.FetchAGame, async () => {
            await this.getGame();
        });
    }

    async getGame() {
        if (this.gameId) {
            let result = await this.gameRepository.get(this.gameId);
            this.game = result.payload;
            if (this.game && this.game['status'] === GameStatus.AwaitingPlayers) {
                this.router.navigateBack();
            }
            this.render();
        }
    }

    render() {
        this.game.rounds.forEach(round => {
            if (round.status === RoundStatus.NotStarted) {
                round.startButtonCaption = 'Start round';
                round.enableStartControl = false;
                round.enableFinishControl = false;
                round.displayFinishControl = true;
                round.headerClass = 'panel-default';
            }

            if (round.status === RoundStatus.AwaitingPlayers) {
                round.startButtonCaption = 'Awaiting';
                round.enableStartControl = false;
                round.enableFinishControl = false;
                round.displayFinishControl = true;
                round.headerClass = 'panel-default';
            }

            if (round.status === RoundStatus.ReadyToStart) {
                round.startButtonCaption = 'Start round';
                round.enableStartControl = true;
                round.enableFinishControl = false;
                round.displayFinishControl = true;
                round.headerClass = 'panel-default';
            }

            if (round.status === RoundStatus.InProgress) {
                round.startButtonCaption = 'Started';
                round.enableStartControl = false;
                round.enableFinishControl = true;
                round.displayFinishControl = true;
                round.headerClass = 'panel-default';
            }

            if (round.status === RoundStatus.AwaitingConfirmation) {
                round.startButtonCaption = 'Awaiting';
                round.enableStartControl = false;
                round.enableFinishControl = true;
                round.displayFinishControl = false;
                round.headerClass = 'panel-default';
            }

            if (round.status === RoundStatus.Finished) {
                round.startButtonCaption = 'Finished';
                round.enableStartControl = false;
                round.enableFinishControl = true;
                round.displayFinishControl = true;
                if (this.user.username === round.winner) {
                    round.headerClass = 'panel-success';
                } else {
                    round.headerClass = 'panel-danger';
                }
                round.timePlayed = moment.utc(
                        moment(round.finishDate)
                            .diff(
                                moment(round.startDate)))
                            .format("HH:mm:ss");
            }         

        });
        // if (this.game.status === GameStatus.InProgress) {

        // }
        //==============================
        // let firstRoundStarted = true;
        // if (!this.game.rounds[0].dateStarted) {
        //     firstRoundStarted = false;
        //     this.game.rounds[0]['enableStartControl'] = true;
        // }

        // for(let i = 0; i < this.game.rounds.length; i ++) {
        //     if(this.game.rounds[i].dateFinished) {
        //             this.game.rounds[i]['timePlayed'] = moment.utc(
        //                 moment(this.game.rounds[i].dateFinished)
        //                     .diff(
        //                         moment(this.game.rounds[i].dateStarted)))
        //                     .format("HH:mm:ss");
        //     }
        //     if (this.game.winner) {
        //         this.game.rounds[i]['enableStartControl'] = false;
        //         this.game.rounds[i]['enableFinishControl'] = false;
        //     } else {
        //         this.game.rounds[i]['header-class'] = 'panel-default';
        //         if (this.game.rounds[i].dateStarted) {
        //             this.game.rounds[i]['enableStartControl'] = false;
        //             this.game.rounds[i]['enableFinishControl'] = true;
        //             this.game.rounds[i]['header-class'] = 'panel-primary';
        //         } else {
        //             if (this.game.rounds[i-1] && this.game.rounds[i-1].dateFinished) {
        //                 this.game.rounds[i]['enableStartControl'] = true;
        //             }
        //         }

        //         if(this.game.rounds[i].dateFinished) {
        //             this.game.rounds[i]['enableFinishControl'] = false;
        //             if (this.game.rounds[i].winner === this.user.username) {
        //                 this.game.rounds[i]['header-class'] = 'panel-success';
        //             } else {
        //                 this.game.rounds[i]['header-class'] = 'panel-danger';
        //             }
        //         }
        //     }
        // }

        // this.status = this.getStatus(this.game.isComplete, this.game.isPendingDecision);
    }

    // getStatus(isComplete, isPendingDecision): string {
    //     if (isComplete && !isPendingDecision) {
    //         return 'Finished';
    //     } 

    //     if (isComplete && isPendingDecision) {
    //         return 'Pending Decision';
    //     }
        
    //     return 'In Progress';
    // }
    
    async startRound(roundId: string) {
    let canProceed = false;
    await this.dialogService
        .open({viewModel: ConfirmationDialog, model: `Begin the round?`})
        .then(response => {
            if (!response.wasCancelled) {
                canProceed = true;
            }
        });
        if (canProceed) {
            let response = await this.gameRepository.startRound(this.game['_id'] , roundId);
            if (response['success']) {
                await this.getGame();
            }
        }
    }

    async finishRound(roundId: number, name: string) {
        let canProceed = false;
        await this.dialogService
            .open({viewModel: ConfirmationDialog, model: `Are you sure ${name} won the game?`})
            .then(response => {
                if (!response.wasCancelled) {
                    canProceed = true;
                }
            });
        if (canProceed) {
            let response = await this.gameRepository.finishRound(this.game['_id'] , roundId, name);
            if (response['success']) {
                await this.getGame();
            }            
            // round.winner = name;
            // round.dateFinished = moment().toISOString();
            // this.render();
            // let response = await this.gameRepository.update(this.game['_id'] ,this.game);
            
            // if (response.finished) {
            //     this.game.winner = response.payload.winner;
            //     this.game.isComplete = response.payload.isComplete;
            //     this.game.isPendingDecision = response.payload.isPendingDecision;
            //     this.render();
            // }
        }
    }

}