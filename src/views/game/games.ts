import { GameStatus } from './../../enums/game-status-enum';
import { Events } from './../../enums/events';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';
import { GameRepository } from './../../repositories/gameRepository';
import { Game } from './../../models/game';
import { User } from './../../models/user';
import { autoinject, bindable } from 'aurelia-framework';
import * as moment from 'moment';

@autoinject
export class Games {
    games: Game[];
    filteredGames: Game[] = [];
    @bindable showInProgress: boolean = false;
    @bindable showFinished: boolean = false;
    
    constructor(private user: User,
                private router: Router,
                private gameRepository: GameRepository,
                private ea: EventAggregator) {
        this.setupSubscriptions();
    }
    
    setupSubscriptions() {
        this.ea.subscribe(Events.FetchGames, async () => {
            await this.getGames();
        });
    }

    async getGames() {
        this.games = await this.gameRepository.getAll();
        this.games.forEach(game => game['statusString'] = this.getStatus(game['status']));
        this.filteredGames = this.games;
        console.log('games:', this.filteredGames);
    }

    async activate(params, routeConfig, navigationInstruction) {
        await this.getGames();
    }

    getStatus(status: string): string {
        if (status === GameStatus.AwaitingPlayers) {
            return 'Awaiting Players';
        }

        if (status === GameStatus.Finished) {
            return 'Finished';
        }

        if (status === GameStatus.InProgress) {
            return 'In progress';
        }                

        if (status === GameStatus.Indecisive) {
            return 'Indecisive';
        }        

        return '';
    }

    selectGame(game: Game) {
        if (game['status'] !== GameStatus.AwaitingPlayers) {
            this.router.navigateToRoute('game-play', {gameId: game['_id']});
        }
    }

    newGame() {
        this.router.navigateToRoute('game-add');
    }
}