import { System } from './../../models/system';
import { SystemRepository } from './../../repositories/systemRepository';
import { Router } from 'aurelia-router';
import { autoinject } from 'aurelia-framework';

@autoinject
export class Admin {
    
    constructor(
        private router: Router,
        private systemRepository: SystemRepository,
        private system: System) {
    }

    cancel() {
        this.router.navigateToRoute('dashboard');
    }

    async save() {
        let response = await this.systemRepository.update(this.system);

        this.router.navigateToRoute('dashboard');
    }    
}