import { MessageType } from './../../enums/message-type-enum';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Events } from './../../enums/events';
import { NotificationRepository } from './../../repositories/notificationRepository';
import { Message } from './../../models/message';
import { autoinject } from 'aurelia-framework';

@autoinject
export class Notifications {
    notifications: Message[];
    fetching: boolean = false;

    constructor(
        private notificationRepository: NotificationRepository,
        private ea: EventAggregator) {
        this.setupSubscriptions();
    }

    setupSubscriptions() {
        this.ea.subscribe(Events.FetchNotifications, async () => {
            await this.getNotifications();
        });
    }

    open(notification: any) {
        if (!notification.expand) {
            notification.expand = true;
        } else {
            notification.expand = false;
        }
    }

    async getNotifications() {
        this.fetching = true;
        this.notifications = await this.notificationRepository.getAll();
        setTimeout( ()=> {
            this.fetching = false;
        }, 2000);
    }

    async activate(params, routeConfig, navigationInstruction) {
        await this.getNotifications();
    }

    async accept(notification: any) {
        if (notification.messageType === MessageType.JoinGame) {
            let response = await this.notificationRepository.join(notification.gameId, {notificationId: notification._id});
            if (response['success']) {
                await this.getNotifications();
            }
        }

        if (notification.messageType === MessageType.StartRound) {
            let response = await this.notificationRepository.startRound(notification.gameId, notification.roundId, {notificationId: notification._id});
            if (response['success']) {
                await this.getNotifications();
            }
        }        

        if (notification.messageType === MessageType.FinishRound) {
            let response = await this.notificationRepository.finishRound(notification.gameId, notification.roundId, {notificationId: notification._id});
            if (response['success']) {
                await this.getNotifications();
            }
        }         
    }

    async decline() {

    }
}