export class Message {
  dateCreated: string;
  message: string;
  messageType: string
}