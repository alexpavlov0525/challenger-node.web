export class Round {
    startDate: string;
    finishDate: string;
    status: number;
    winner: string;
    startButtonCaption: string;
    enableStartControl: boolean;
    enableFinishControl: boolean;
    displayFinishControl: boolean;
    headerClass: string;
    timePlayed: string;
}