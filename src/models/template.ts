export class Template {
    constructor(id: string = '-1',
                name: string | null = null,
                description: string | null = null,
                createdBy: string | null = null,
                minPlayers: number | null = null,
                maxPlayers: number | null = null,
                rounds: number | null = null,
                roundsToWin: number | null = null) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdBy = createdBy;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.rounds = rounds;
        this.roundsToWin = roundsToWin;
    }
    id: string;
    name: string | null;
    description: string | null;
    createdBy: string | null;
    minPlayers: number | null;
    maxPlayers: number | null;
    rounds: number | null;
    roundsToWin: number | null;
}