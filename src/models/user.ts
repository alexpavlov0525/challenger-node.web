export class User{
    username: string;
    roles: string[];
    isLoggedIn: boolean;
}