import { Round } from './round';
import { Template } from './template';

export class Game {
    dateCreated: Date;
    dateStarted: Date;
    dateFinished: Date;
    isComplete: boolean;
    isPendingDecision: boolean;
    templateId: string;
    winner: string;
    rounds: Round[];
    players: string[];
    readyPlayers: string[];
    status: string;
}