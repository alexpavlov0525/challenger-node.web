import { CustomControl } from './customControl';


export abstract class EntityViewModel {
    childControls: CustomControl [] = [];
    isAddingNew: boolean = false;
    forceDeactivate: boolean = false;
    entityId: string;
    errors: string[];
    canBeDeactivated: boolean = true;

    registerChild(childControl: CustomControl) {
        console.log('registered ', childControl.property);
        this.childControls.push(childControl);
    }

    async validateChildren() {
        let isValid = true;

        await this.childControls.forEach(async (child) => {
            let isChildValid = await child.validate();
            if (!isChildValid) {
                isValid = false;
            }
        })

        console.log('can deactivate:', isValid);

        return isValid;
    }

    async canDeactivate() {
        if (this.forceDeactivate) {
            return true;
        }
        return await this.validateChildren() && this.canBeDeactivated;
    }
}