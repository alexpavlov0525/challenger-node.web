import { NotificationRepository } from './repositories/notificationRepository';
import { Notification } from './dialogs/notification';
import { Configuration } from './configuration';
import { Roles } from './enums/roles';
import { Events } from './enums/events';
import { User } from './models/user';
import { UserRepository } from './repositories/user/userRepository';
import { Aurelia, autoinject, Container } from 'aurelia-framework';
import {AppRouter, RouterConfiguration, Redirect} from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as jwt from 'jwt-decode';
import * as socketio from 'socket.io-client';
import * as moment from 'moment';

@autoinject
export class App {
  router: AppRouter;
  private isAdmin: boolean;
  socketio: any;
  counter: number = 0;

  constructor(
    private userRepository: UserRepository,
    private user: User,
    private configuration: Configuration,
    private eventAggregator: EventAggregator,
    private notification: Notification,
    private notificationRepository: NotificationRepository) {
      this.setupSubscriptions();
  }

  setupSocketConnection() {
    let token = localStorage.getItem('chnt');
    if (!token) {
      console.log('No token! Can\'t connect');
      return;
    }
    this.socketio = socketio(`${this.configuration.notificationBaseUri}?token=${token}&username=${this.user.username}`);    
    this.socketio.on('message', (payload) => {
      this.notification.info(payload);
      this.fireEvents();
      console.log('sever says: ', payload);
    });
    this.socketio.on('refresh', (message) => {
      this.fireEvents();
      console.log('sever says: ', message);
    });    
  }

  fireEvents() {
      this.eventAggregator.publish(Events.FetchGames);
      this.eventAggregator.publish(Events.GetInboxCounter);
      this.eventAggregator.publish(Events.FetchNotifications);
      this.eventAggregator.publish(Events.FetchAGame);
  }

  setupSubscriptions() {
    this.eventAggregator.subscribe(Events.Logout, () => {
      this.socketio.close();
      this.logout();
    });

    this.eventAggregator.subscribe(Events.LoggedIn, async () => {
      this.setupSocketConnection();
      this.user.isLoggedIn = true;
      if (this.user.roles && this.user.roles.indexOf(Roles.Admin) > -1) {
        this.isAdmin = true;
      } else {
        this.isAdmin = false;
      }
      this.counter = await this.notificationRepository.getCounter();
    });

    this.eventAggregator.subscribe(Events.GetInboxCounter, async () => {
      this.counter = await this.notificationRepository.getCounter();
    });
  }

  logout() {
    localStorage.removeItem('chnt');
    this.user.isLoggedIn = false;
    this.router.navigateToRoute('login');
  }

  admin() {
    this.router.navigateToRoute('admin');
  }

  notifications() {
    this.router.navigateToRoute('notifications');
  }

  configureRouter(config: RouterConfiguration, router: AppRouter) {
    config.title = 'Challenger Node';
    let authorizeStep = new AuthorizeStep;
    config.addAuthorizeStep(authorizeStep);
    config.map([
      {
        route: ['', 'welcome'],
        name: 'welcome',
        moduleId: 'views/welcome/welcome',
        nav: false,
        title: 'Welcome'
      },
      { 
        route: 'dashboard', 
        name: 'dashboard',      
        moduleId: './dashboard',      
        nav: true, 
        title: 'Dashboard' 
      },
      {
        route: 'login',
        name: 'login',
        moduleId: 'views/login/login',
        nav: false,
        title: 'Login'
      },
      { 
        route: 'game',          
        name: 'games',         
        moduleId: 'views/game/games',      
        nav: true, 
        title: 'Games' 
      },
      { 
        route: 'game/:gameId',          
        name: 'game-play',         
        moduleId: 'views/game/game-play',      
        nav: false, 
        title: 'Game' 
      },    
      { 
        route: 'game/add',          
        name: 'game-add',         
        moduleId: 'views/game/game-edit',      
        nav: false, 
        title: 'Game' 
      },         
      {
        route: 'admin',          
        name: 'admin',         
        moduleId: 'views/admin/admin',      
        nav: false, 
        title: 'Admin'         
      },         
      {
        route: 'template',          
        name: 'templates',         
        moduleId: 'views/template/templates',      
        nav: true, 
        title: 'Templates'         
      },   
      {
        route: 'template/:id',          
        name: 'template',         
        moduleId: 'views/template/template-edit',      
        nav: false, 
        title: 'Template'         
      },
      {
        route: 'template/add',          
        name: 'template-add',         
        moduleId: 'views/template/template-edit',      
        nav: false, 
        title: 'Add New Template'         
      },
      {
        route: 'notification',          
        name: 'notifications',         
        moduleId: 'views/notifications/notifications',      
        nav: false, 
        title: 'Notifications'         
      }                  
    ]);

    this.router = router;
  }
}

class AuthorizeStep {
  user: User;
  ea: EventAggregator;

  constructor() {
    this.user = Container.instance.get(User);
    this.ea = Container.instance.get(EventAggregator);
  }

  run(navigationInstruction, next) {
    if (navigationInstruction.config.route !== '') {
      if (navigationInstruction.config.route !== 'login') {
        let token = localStorage.getItem('chnt');
        if (!token) {
          return next.cancel(new Redirect('login'));
        }
        let decoded = jwt(token);
        this.user.username = decoded.username;
        this.user.roles = decoded.roles;
        if (!this.user.isLoggedIn) {
          this.ea.publish(Events.LoggedIn);
        }
      } else {
        let token = localStorage.getItem('chnt');
        if (token) {
          let decoded = jwt(token);
          if (moment().unix() < decoded.exp) {
            return next.cancel(new Redirect('dashboard'));    
          }
        }
      }
    }
    
    if (navigationInstruction.config.route === 'admin') {
      if (this.user.roles && this.user.roles.indexOf(Roles.Admin) > -1) {
        return next();
      } else {
        return next.cancel(new Redirect('dashboard'));
      }
    }
    return next();
  }
}
