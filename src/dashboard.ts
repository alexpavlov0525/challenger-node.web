import { DashboardRepository } from './repositories/dashboardRepository';
import { User } from './models/user';
import { Router } from 'aurelia-router';
import { autoinject, bindable } from 'aurelia-framework';

@autoinject
export class Welcome {
  @bindable stat: any;
  constructor(
    private router: Router,
    private user: User,
    private dashboardRepository: DashboardRepository) {

  }

  heading: string = 'Welcome, ';

  createNewGame() {
    this.router.navigateToRoute("game-add");
  }

  async activate(params, routeConfig, navigationInstruction) {
    this.stat = await this.dashboardRepository.getAll();
  }

}

export class UpperValueConverter {
  toView(value: string): string {
    return value && value.toUpperCase();
  }
}
