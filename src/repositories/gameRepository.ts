import { Game } from './../models/game';
import { WebApi } from './../services/web-api';
import { autoinject } from 'aurelia-framework';

@autoinject
export class GameRepository {
    constructor(private api: WebApi) {

    }

    async getAll(): Promise<any[]> {
        let result = await this.api.getAll('game');
        return result['games'];
    }

    async get(id: string): Promise<any> {
        return await this.api.get(`game/${id}`);
    }

    async insert(entity: any): Promise<any> {
        return await this.api.post('game', entity);
    }    

    async startRound(gameId: string, roundId: string) {
        return await this.api.put(`game/${gameId}/round/${roundId}/start`, {});
    }

    async finishRound(gameId: string, roundId: number, name: string) {
        return await this.api.put(`game/${gameId}/round/${roundId}/finish`, {winner: name});
    }    
}