import {autoinject} from 'aurelia-framework';
import {AuthApi} from '../../services/auth-api';

@autoinject
export class AuthRepository {
    constructor(private api: AuthApi) {
        //
    }

    async authenticate(payload: any): Promise<any> {
      let result = await this.api.post('authenticate', payload);
      return result;
    }

    async add(user: any): Promise<any> {
        let result = await this.api.post('create', user);
        return result;
    }
}
