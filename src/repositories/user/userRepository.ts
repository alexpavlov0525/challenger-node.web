import {autoinject} from 'aurelia-framework';
import {WebApi} from '../../services/web-api';

@autoinject
export class UserRepository {
    constructor(private api: WebApi) {
        //
    }

    async getAllUsernames(): Promise<any[]> {
        let result = await this.api.getAll('user');
        return result['users'];
    }

    async getSelf() {
        return await this.api.get('user/get/profile');
    }
}
