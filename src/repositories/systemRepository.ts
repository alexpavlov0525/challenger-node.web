import { WebApi } from './../services/web-api';
import { autoinject } from 'aurelia-framework';

@autoinject
export class SystemRepository {
    constructor(private api: WebApi) {
        //
    }

    async getCanCreate() {
        return await this.api.getAll('system/cancreate');
    }

    async update(system: any) {
        return await this.api.put('system', system);
    }
}