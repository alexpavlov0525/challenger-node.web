import { Game } from './../models/game';
import { WebApi } from './../services/web-api';
import { autoinject } from 'aurelia-framework';

@autoinject
export class DashboardRepository {
    constructor(private api: WebApi) {

    }

    async getAll(): Promise<any[]> {
        let result = await this.api.getAll('dashboard');
        return result['stat'];
    }
}