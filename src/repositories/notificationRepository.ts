import { WebApi } from './../services/web-api';
import { autoinject } from 'aurelia-framework';

@autoinject
export class NotificationRepository {
    constructor(private api: WebApi) {

    }

    async getAll() {
        let result = await this.api.getAll('notification');
        return result['notifications'];
    }    

    async getCounter() {
        let result = await this.api.get('notification/counter');
        return result['counter'];
    }

    async join(id: string, payload: any): Promise<any> {
        let result = await this.api.put(`game/${id}/join`, payload);
        return result;
    }

    async startRound(gameId: string, roundId: number, payload: any) {
        let result = await this.api.put(`game/${gameId}/round/${roundId}/start/agree`, payload);
        return result;        
    }

    async finishRound(gameId: string, roundId: number, payload: any) {
        let result = await this.api.put(`game/${gameId}/round/${roundId}/finish/agree`, payload);
        return result;        
    }    
}