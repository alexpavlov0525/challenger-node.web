import { autoinject } from 'aurelia-framework';
import { Template } from './../models/template';
import { WebApi } from './../services/web-api';

@autoinject
export class TemplateRepository {
    constructor(private api: WebApi) {
    }

    async getAll(): Promise<any[]> {
        let result = await this.api.getAll('template');
        return result['templates'];
    }

    async get(id: string): Promise<any> {
        let template = await this.api.get(`template/${id}`);
        return template;
    }

    async update(id: string, entity: any) {
        let response = await this.api.put(`template/${id}`, entity);
        return response;
    }

    async insert(entity: any) {
        return await this.api.post('template', entity);
    }

}