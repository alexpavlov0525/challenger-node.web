export class Events {
    static get Logout(): string {
        return 'logout';
    }

    static get LoggedIn(): string {
        return 'loggedIn';
    }

    static get FetchGames(): string {
        return 'fetchGames';
    }        

    static get FetchAGame(): string {
        return 'fetchAGame';
    }       

    static get GetInboxCounter(): string {
        return 'getInboxCounter';
    }        

    static get FetchNotifications(): string {
        return 'FetchNotifications';
    }    
}