export class MessageType {
  static get JoinGame(): string {
    return 'JoinGame';
  }

  static get StartRound(): string {
    return 'StartRound';
  }

  static get FinishRound(): string {
    return 'FinishRound';
  }
}