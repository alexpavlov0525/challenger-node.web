export class GameStatus {
  static get AwaitingPlayers(): string {
    return 'AwaitingPlayers';
  }

  static get InProgress(): string {
    return 'InProgress';
  }

  static get Finished(): string {
    return 'Finished';
  }

  static get Indecisive(): string {
    return 'Indecisive';
  }
}